/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Compc      eqc             Equal constant
      Compc      nec             Not equal constant
      Compc      lec             Less equal constant
      Compc      ltc             Less then constant
      Compc      gec             Greater equal constant
      Compc      gtc             Greater then constant
*/

#include <cdi.h>

#include "process_int.h"
#include "cdo_vlist.h"
#include "param_conversion.h"

static
auto func_compc = [](auto hasMissvals, auto n, auto mv, const auto &vIn, const auto cVal, auto &vOut, auto binray_operator)
{
  if (hasMissvals)
    {
      const auto c_is_missval = dbl_is_equal(cVal, mv);
      if (std::isnan(mv))
        for (size_t i = 0; i < n; ++i) vOut[i] = (dbl_is_equal(vIn[i], mv) || c_is_missval) ? mv : binray_operator(vIn[i], cVal);
      else
        for (size_t i = 0; i < n; ++i) vOut[i] = (is_equal(vIn[i], mv) || c_is_missval) ? mv : binray_operator(vIn[i], cVal);
    }
  else
    {
      for (size_t i = 0; i < n; ++i) vOut[i] = binray_operator(vIn[i], cVal);
    }
};

void *
Compc(void *process)
{
  cdo_initialize(process);

  const auto EQC = cdo_operator_add("eqc", 0, 0, nullptr);
  const auto NEC = cdo_operator_add("nec", 0, 0, nullptr);
  const auto LEC = cdo_operator_add("lec", 0, 0, nullptr);
  const auto LTC = cdo_operator_add("ltc", 0, 0, nullptr);
  const auto GEC = cdo_operator_add("gec", 0, 0, nullptr);
  const auto GTC = cdo_operator_add("gtc", 0, 0, nullptr);

  const auto operatorID = cdo_operator_id();

  operator_input_arg("constant value");
  const auto rc = parameter_to_double(cdo_operator_argv(0));

  const auto streamID1 = cdo_open_read(0);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = vlistDuplicate(vlistID1);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = taxisDuplicate(taxisID1);
  vlistDefTaxis(vlistID2, taxisID2);

  VarList varList;
  varListInit(varList, vlistID1);

  const auto gridsizemax = vlistGridsizeMax(vlistID1);
  Varray<double> vaIn(gridsizemax), vaOut(gridsizemax);

  const auto streamID2 = cdo_open_write(1);
  cdo_def_vlist(streamID2, vlistID2);

  int tsID = 0;
  while (true)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      cdo_taxis_copy_timestep(taxisID2, taxisID1);
      cdo_def_timestep(streamID2, tsID);

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);
          size_t nmiss;
          cdo_read_record(streamID1, vaIn.data(), &nmiss);

          const auto missval = varList[varID].missval;
          const auto ngp = varList[varID].gridsize;
          const auto datatype = varList[varID].datatype;
          const double rcv = (datatype == CDI_DATATYPE_FLT32) ? (float) rc : rc;

          if (nmiss > 0) cdo_check_missval(missval, varList[varID].name);

          const auto hasMissvals = (nmiss > 0 || DBL_IS_EQUAL(rc, missval));
          // clang-format off
          if      (operatorID == EQC) func_compc(hasMissvals, ngp, missval, vaIn, rcv, vaOut, binary_op_EQ);
          else if (operatorID == NEC) func_compc(hasMissvals, ngp, missval, vaIn, rcv, vaOut, binary_op_NE);
          else if (operatorID == LEC) func_compc(hasMissvals, ngp, missval, vaIn, rcv, vaOut, binary_op_LE);
          else if (operatorID == LTC) func_compc(hasMissvals, ngp, missval, vaIn, rcv, vaOut, binary_op_LT);
          else if (operatorID == GEC) func_compc(hasMissvals, ngp, missval, vaIn, rcv, vaOut, binary_op_GE);
          else if (operatorID == GTC) func_compc(hasMissvals, ngp, missval, vaIn, rcv, vaOut, binary_op_GT);
          else cdo_abort("Operator not implemented!");
          // clang-format on

          nmiss = varray_num_mv(ngp, vaOut, missval);
          cdo_def_record(streamID2, varID, levelID);
          cdo_write_record(streamID2, vaOut.data(), nmiss);
        }

      tsID++;
    }

  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
