/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Ralf Müller

*/

#include <cdi.h>

#include "process_int.h"
#include "cdi_lockedIO.h"
#include <mpim_grid.h>
#include "griddes.h"

// read only the first data variable from input filename into a given double pointer
static void
read_first_record(const char *filename, double *field)
{
  size_t nmiss;
  int varID, levelID;
  const auto streamID = stream_open_read_locked(filename);
  streamInqTimestep(streamID, 0);
  streamInqRecord(streamID, &varID, &levelID);
  streamReadRecord(streamID, field, &nmiss);
  streamClose(streamID);
}

// count the number of locations, for which the mask is true
static int
countMask(const double *maskField, size_t gridSize, double falseVal)
{
  size_t counter = 0;

  for (size_t i = 0; i < gridSize; ++i)
    {
      if (!DBL_IS_EQUAL(maskField[i], falseVal)) counter++;
    }

  return counter;
}

/*
 * the operators argument has to be a single horizontal field,
 * non-zero values are used to mark the relevant locations
 */
void *
MapReduce(void *process)
{
  cdo_initialize(process);

  if (cdo_operator_argc() < 1) cdo_abort("Too few arguments!");

  // check input grid type and size - this will be used for selecting relevant variables from the input file
  const auto maskfilename = cdo_operator_argv(0).c_str();
  const auto inputGridID = cdo_define_grid(maskfilename);
  const auto inputGridSize = gridInqSize(inputGridID);
  const auto inputGridType = gridInqType(inputGridID);
  Debug(cdoDebug, "MapReduce: input gridSize: %zu", inputGridSize);

  // create an index list of the relevant locations
  std::vector<size_t> maskIndexList;
  size_t maskSize;
  {
    Varray<double> inputMaskField(inputGridSize);
    read_first_record(maskfilename, inputMaskField.data());

    // non-zero values mark the relevant points
    maskSize = countMask(inputMaskField.data(), inputGridSize, 0.0);
    Debug(cdoDebug, "MapReduce: maskSize = %zu", maskSize);

    maskIndexList.resize(maskSize, -1);

    size_t k = 0;
    for (size_t i = 0; i < inputGridSize; ++i)
      {
        if (!DBL_IS_EQUAL(inputMaskField[i], 0.0)) maskIndexList[k++] = i;
      }

    if (k == inputGridSize) cdo_warning("Number of mask values and gridsize is the same, no reduction!");
  }

  // check if coordinated bounds shound not be created
  bool nobounds = false, nocoords = false;
  if (2 <= cdo_operator_argc())
    {
      const auto coordinatesLimitation = cdo_operator_argv(1).c_str();
      if (0 == strncmp("nobounds", coordinatesLimitation, 8)) nobounds = true;
      if (0 == strncmp("nocoords", coordinatesLimitation, 8)) nocoords = true;
    }
  // create unstructured output grid including bounds
  const auto outputGridID = gridToUnstructuredSelecton(inputGridID, maskSize, maskIndexList.data(), nocoords, nobounds);

  /* create output vlist: Only variabes which have the same gridtype and
   * gridsize as the input mask should be proessed. Everything else is ignoreds
   * {{{ */
  const auto streamID1 = cdo_open_read(0);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto nvars = vlistNvars(vlistID1);
  std::vector<bool> vars(nvars, false);

  VarList varList1;
  varListInit(varList1, vlistID1);

  // use vlist flags for marking the corresponding variables
  vlistClearFlag(vlistID1);
  for (int varID = 0; varID < nvars; ++varID)
    {
      const auto gridID = varList1[varID].gridID;
      if (inputGridType == gridInqType(gridID) && inputGridSize == gridInqSize(gridID))
        {
          vars[varID] = true;
          const auto nlevels = varList1[varID].nlevels;
          for (int levID = 0; levID < nlevels; levID++) vlistDefFlag(vlistID1, varID, levID, true);
        }
      else
        {
          cdo_warning("Gridtype or gridsize differ, skipped variable %s!", varList1[varID].name);
        }
    }

  const auto vlistID2 = vlistCreate();
  cdo_vlist_copy_flag(vlistID2, vlistID1);
  vlistDefNtsteps(vlistID2, vlistNtsteps(vlistID1));
  // }}}

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = taxisDuplicate(taxisID1);
  vlistDefTaxis(vlistID2, taxisID2);

  // use the new selection grid for all output variables
  const auto ngrids = vlistNgrids(vlistID2);
  for (int index = 0; index < ngrids; ++index) vlistChangeGridIndex(vlistID2, index, outputGridID);

  // loop over input fields and mask the data values {{{
  const auto streamID2 = cdo_open_write(1);
  cdo_def_vlist(streamID2, vlistID2);

  Varray<double> arrayIn(inputGridSize), arrayOut(maskSize);

  int tsID = 0;
  while (true)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      cdo_taxis_copy_timestep(taxisID2, taxisID1);
      cdo_def_timestep(streamID2, tsID);

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);
          if (vars[varID])
            {
              const auto varID2 = vlistFindVar(vlistID2, varID);
              const auto levelID2 = vlistFindLevel(vlistID2, varID, levelID);

              size_t nmiss;
              cdo_read_record(streamID1, arrayIn.data(), &nmiss);

              for (size_t i = 0; i < maskSize; ++i) arrayOut[i] = arrayIn[maskIndexList[i]];

              cdo_def_record(streamID2, varID2, levelID2);
              cdo_write_record(streamID2, arrayOut.data(), 0);
            }
        }
      tsID++;
    }
  // }}}

  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
