/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida
          Ralf Quast

*/

/*
   This module contains the following operators:

      Runpctl    runpctl         Running percentiles
*/

#include <cdi.h>

#include "process_int.h"
#include "param_conversion.h"
#include "percentiles.h"
#include "datetime.h"

void *
Runpctl(void *process)
{
  const auto timestat_date = TimeStat::MEAN;

  cdo_initialize(process);

  operator_input_arg("percentile number, number of timesteps");
  operator_check_argc(2);
  const auto pn = parameter_to_double(cdo_operator_argv(0));
  const auto ndates = parameter_to_int(cdo_operator_argv(1));

  const auto streamID1 = cdo_open_read(0);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = vlistDuplicate(vlistID1);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = taxisDuplicate(taxisID1);
  taxisWithBounds(taxisID2);
  vlistDefTaxis(vlistID2, taxisID2);

  const auto streamID2 = cdo_open_write(1);
  cdo_def_vlist(streamID2, vlistID2);

  const auto nvars = vlistNvars(vlistID1);

  const auto maxrecs = vlistNrecs(vlistID1);
  std::vector<RecordInfo> recList(maxrecs);

  DateTimeList dtlist;
  dtlist.set_stat(timestat_date);
  dtlist.set_calendar(taxisInqCalendar(taxisID1));

  Varray<float> array_f(ndates);
  Varray<double> array_d(ndates);
  FieldVector3D vars1(ndates + 1);

  for (int its = 0; its < ndates; its++) fields_from_vlist(vlistID1, vars1[its]);

  VarList varList1;
  varListInit(varList1, vlistID1);

  int tsID;
  for (tsID = 0; tsID < ndates; ++tsID)
    {
      auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) cdo_abort("File has less than %d timesteps!", ndates);

      dtlist.taxis_inq_timestep(taxisID1, tsID);

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);

          if (tsID == 0)
            {
              recList[recID].varID = varID;
              recList[recID].levelID = levelID;
              recList[recID].lconst = (varList1[varID].timetype == TIME_CONSTANT);
            }

          auto &field = vars1[tsID][varID][levelID];
          field.init(varList1[varID]);
          cdo_read_record(streamID1, field);
        }
    }

  int otsID = 0;
  while (true)
    {
      for (int varID = 0; varID < nvars; ++varID)
        {
          if (varList1[varID].timetype == TIME_CONSTANT) continue;

          const auto gridsize = varList1[varID].gridsize;
          const auto nlevels = varList1[varID].nlevels;
          const auto missval = varList1[varID].missval;

          for (int levelID = 0; levelID < nlevels; ++levelID)
            {
              auto &field1 = vars1[0][varID][levelID];
              size_t nmiss = 0;
              if (field1.memType == MemType::Float)
                {
                  for (size_t i = 0; i < gridsize; ++i)
                    {
                      int j = 0;
                      for (int inp = 0; inp < ndates; ++inp)
                        {
                          const auto val = vars1[inp][varID][levelID].vec_f[i];
                          if (!DBL_IS_EQUAL(val, (float) missval)) array_f[j++] = val;
                        }

                      if (j > 0)
                        {
                          field1.vec_f[i] = percentile(array_f.data(), j, pn);
                        }
                      else
                        {
                          field1.vec_f[i] = missval;
                          nmiss++;
                        }
                    }
                }
              else
                {
                  for (size_t i = 0; i < gridsize; ++i)
                    {
                      int j = 0;
                      for (int inp = 0; inp < ndates; ++inp)
                        {
                          const auto val = vars1[inp][varID][levelID].vec_d[i];
                          if (!DBL_IS_EQUAL(val, missval)) array_d[j++] = val;
                        }

                      if (j > 0)
                        {
                          field1.vec_d[i] = percentile(array_d.data(), j, pn);
                        }
                      else
                        {
                          field1.vec_d[i] = missval;
                          nmiss++;
                        }
                    }
                }
              field1.nmiss = nmiss;
            }
        }

      dtlist.stat_taxis_def_timestep(taxisID2, ndates);
      cdo_def_timestep(streamID2, otsID);

      for (int recID = 0; recID < maxrecs; ++recID)
        {
          if (otsID && recList[recID].lconst) continue;

          const auto varID = recList[recID].varID;
          const auto levelID = recList[recID].levelID;

          cdo_def_record(streamID2, varID, levelID);
          auto &field1 = vars1[0][varID][levelID];
          cdo_write_record(streamID2, field1);
        }

      otsID++;

      dtlist.shift();

      vars1[ndates] = vars1[0];
      for (int inp = 0; inp < ndates; ++inp) vars1[inp] = vars1[inp + 1];

      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      dtlist.taxis_inq_timestep(taxisID1, ndates - 1);

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);
          auto &fieldN = vars1[ndates - 1][varID][levelID];
          cdo_read_record(streamID1, fieldN);
        }

      tsID++;
    }

  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
