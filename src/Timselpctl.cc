/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Copyright (C) 2006 Brockmann Consult

  Author: Ralf Quast
          Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Timselpctl    timselpctl         Time range percentiles
*/

#include <cdi.h>

#include "cdo_options.h"
#include "process_int.h"
#include "cdo_vlist.h"
#include "param_conversion.h"
#include "percentiles_hist.h"
#include "datetime.h"

void *
Timselpctl(void *process)
{
  const auto timestat_date = TimeStat::MEAN;

  cdo_initialize(process);

  cdo_operator_add("timselpctl", FieldFunc_Pctl, 0, nullptr);

  operator_input_arg("percentile number, nsets <,noffset <,nskip>>");

  const auto nargc = cdo_operator_argc();
  if (nargc < 2) cdo_abort("Too few arguments! Need %d found %d.", 2, nargc);

  const auto pn = parameter_to_double(cdo_operator_argv(0));
  const auto ndates = parameter_to_int(cdo_operator_argv(1));
  int noffset = 0, nskip = 0;
  if (nargc > 2) noffset = parameter_to_int(cdo_operator_argv(2));
  if (nargc > 3) nskip = parameter_to_int(cdo_operator_argv(3));

  if (Options::cdoVerbose) cdo_print("nsets = %d, noffset = %d, nskip = %d", ndates, noffset, nskip);

  const auto streamID1 = cdo_open_read(0);
  const auto streamID2 = cdo_open_read(1);
  const auto streamID3 = cdo_open_read(2);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = cdo_stream_inq_vlist(streamID2);
  const auto vlistID3 = cdo_stream_inq_vlist(streamID3);
  const auto vlistID4 = vlistDuplicate(vlistID1);

  vlist_compare(vlistID1, vlistID2, CMP_ALL);
  vlist_compare(vlistID1, vlistID3, CMP_ALL);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = vlistInqTaxis(vlistID2);
  const auto taxisID3 = vlistInqTaxis(vlistID3);
  // TODO - check that time axes 2 and 3 are equal

  const auto taxisID4 = taxisDuplicate(taxisID1);
  taxisWithBounds(taxisID4);
  vlistDefTaxis(vlistID4, taxisID4);

  const auto streamID4 = cdo_open_write(3);
  cdo_def_vlist(streamID4, vlistID4);

  const auto ntsteps = vlistNtsteps(vlistID1);
  const auto nvars = vlistNvars(vlistID1);

  const auto maxrecs = vlistNrecs(vlistID1);
  std::vector<RecordInfo> recList(maxrecs);

  FieldVector constFields(maxrecs);

  DateTimeList dtlist;
  dtlist.set_stat(timestat_date);
  dtlist.set_calendar(taxisInqCalendar(taxisID1));

  Field field1, field2;

  VarList varList1;
  varListInit(varList1, vlistID1);

  HistogramSet hset(nvars, ntsteps);

  for (int varID = 0; varID < nvars; ++varID) hset.createVarLevels(varID, varList1[varID].nlevels, varList1[varID].gridsize);

  int tsID;
  for (tsID = 0; tsID < noffset; ++tsID)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);

          if (tsID == 0)
            {
              recList[recID].varID = varID;
              recList[recID].levelID = levelID;
              recList[recID].lconst = varList1[varID].timetype == TIME_CONSTANT;
            }
        }
    }

  int otsID = 0;
  if (tsID < noffset)
    {
      cdo_warning("noffset is larger than number of timesteps!");
      goto LABEL_END;
    }

  while (true)
    {
      auto nrecs = cdo_stream_inq_timestep(streamID2, otsID);
      if (nrecs != cdo_stream_inq_timestep(streamID3, otsID))
        cdo_abort("Number of records at time step %d of %s and %s differ!", otsID + 1, cdo_get_stream_name(1),
                  cdo_get_stream_name(2));

      const auto vDateTime2 = taxisInqVdatetime(taxisID2);
      const auto vDateTime3 = taxisInqVdatetime(taxisID3);
      if (cdiDateTime_isNE(vDateTime2, vDateTime3))
        cdo_abort("Verification dates at time step %d of %s and %s differ!", otsID + 1, cdo_get_stream_name(1),
                  cdo_get_stream_name(2));

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID2, &varID, &levelID);
          field1.init(varList1[varID]);
          cdo_read_record(streamID2, field1);

          cdo_inq_record(streamID3, &varID, &levelID);
          field2.init(varList1[varID]);
          cdo_read_record(streamID3, field2);

          hset.defVarLevelBounds(varID, levelID, field1, field2);
        }

      int nsets = 0;
      if (nrecs)
        for (nsets = 0; nsets < ndates; nsets++)
          {
            nrecs = cdo_stream_inq_timestep(streamID1, tsID);
            if (nrecs == 0) break;

            dtlist.taxis_inq_timestep(taxisID1, nsets);

            for (int recID = 0; recID < nrecs; ++recID)
              {
                int varID, levelID;
                cdo_inq_record(streamID1, &varID, &levelID);

                if (tsID == 0)
                  {
                    recList[recID].varID = varID;
                    recList[recID].levelID = levelID;
                    recList[recID].lconst = (varList1[varID].timetype == TIME_CONSTANT);
                  }

                if (tsID == 0 && recList[recID].lconst)
                  {
                    constFields[recID].init(varList1[varID]);
                    cdo_read_record(streamID1, constFields[recID]);
                  }
                else
                  {
                    field1.init(varList1[varID]);
                    cdo_read_record(streamID1, field1);

                    hset.addVarLevelValues(varID, levelID, field1);
                  }
              }

            tsID++;
          }

      if (nrecs == 0 && nsets == 0) break;

      dtlist.stat_taxis_def_timestep(taxisID4, nsets);
      cdo_def_timestep(streamID4, otsID);

      for (int recID = 0; recID < maxrecs; ++recID)
        {
          if (otsID && recList[recID].lconst) continue;

          const auto varID = recList[recID].varID;
          const auto levelID = recList[recID].levelID;
          cdo_def_record(streamID4, varID, levelID);

          if (recList[recID].lconst)
            {
              cdo_write_record(streamID4, constFields[recID]);
            }
          else
            {
              field1.init(varList1[varID]);
              hset.getVarLevelPercentiles(field1, varID, levelID, pn);
              cdo_write_record(streamID4, field1);
            }
        }

      if (nrecs == 0) break;
      otsID++;

      for (int i = 0; i < nskip; ++i)
        {
          nrecs = cdo_stream_inq_timestep(streamID1, tsID);
          if (nrecs == 0) break;
          tsID++;
        }

      if (nrecs == 0) break;
    }

LABEL_END:

  cdo_stream_close(streamID4);
  cdo_stream_close(streamID3);
  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
