/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

*/

#include <cdi.h>

#include "process_int.h"
#include <mpim_grid.h>
#include "cdi_lockedIO.h"

static void
varrms(const Varray<double> &w, const FieldVector &field1, const FieldVector &field2, Field &field3)
{
  const auto grid1 = field1[0].grid;
  const auto grid2 = field2[0].grid;
  const auto missval1 = field1[0].missval;
  const auto missval2 = field2[0].missval;
  double rsum = 0.0, rsumw = 0.0;

  const auto nlev = field1.size();
  const auto len = gridInqSize(grid1);
  if (len != gridInqSize(grid2)) cdo_abort("fields have different size!");

  // if ( nmiss1 )
  {
    for (size_t k = 0; k < nlev; ++k)
      {
        const auto array1 = field1[k].vec_d;
        const auto array2 = field2[k].vec_d;
        for (size_t i = 0; i < len; ++i) /*	  if ( !DBL_IS_EQUAL(w[i], missval1) ) */
          {
            rsum = ADDMN(rsum, MULMN(w[i], MULMN(SUBMN(array2[i], array1[i]), SUBMN(array2[i], array1[i]))));
            rsumw = ADDMN(rsumw, w[i]);
          }
      }
  }
  /*
else
  {
    for ( i = 0; i < len; i++ )
      {
        rsum  += w[i] * array1[i];
        rsumw += w[i];
      }
  }
  */

  const double ravg = SQRTMN(DIVMN(rsum, rsumw));

  size_t rnmiss = 0;
  if (DBL_IS_EQUAL(ravg, missval1)) rnmiss++;

  field3.vec_d[0] = ravg;
  field3.nmiss = rnmiss;
}

void *
Varrms(void *process)
{
  int lastgrid = -1;
  int oldcode = 0;

  cdo_initialize(process);

  operator_check_argc(0);

  const auto needWeights = true;

  const auto streamID1 = cdo_open_read(0);
  const auto streamID2 = cdo_open_read(1);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = cdo_stream_inq_vlist(streamID2);

  double slon = 0.0, slat = 0.0;
  const auto gridID3 = gridCreate(GRID_LONLAT, 1);
  gridDefXsize(gridID3, 1);
  gridDefYsize(gridID3, 1);
  gridDefXvals(gridID3, &slon);
  gridDefYvals(gridID3, &slat);

  vlistClearFlag(vlistID1);
  const auto nvars = vlistNvars(vlistID1);
  for (int varID = 0; varID < nvars; ++varID) vlistDefFlag(vlistID1, varID, 0, true);

  auto vlistID3 = vlistCreate();
  cdo_vlist_copy_flag(vlistID3, vlistID1);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID3 = taxisDuplicate(taxisID1);
  vlistDefTaxis(vlistID3, taxisID3);

  const auto ngrids = vlistNgrids(vlistID1);
  int index = 0;
  const auto gridID1 = vlistGrid(vlistID1, index);

  if (needWeights && gridInqType(gridID1) != GRID_LONLAT && gridInqType(gridID1) != GRID_GAUSSIAN)
    cdo_abort("Unsupported gridtype: %s", gridNamePtr(gridInqType(gridID1)));

  vlistChangeGridIndex(vlistID3, index, gridID3);
  if (ngrids > 1) cdo_abort("Too many different grids!");

  const auto streamID3 = cdo_open_write(2);
  cdo_def_vlist(streamID3, vlistID3);

  FieldVector2D vars1, vars2;
  fields_from_vlist(vlistID1, vars1, FIELD_VEC);
  fields_from_vlist(vlistID2, vars2, FIELD_VEC);

  const auto gridsizemax = vlistGridsizeMax(vlistID1);
  Varray<double> weights;
  if (needWeights) weights.resize(gridsizemax);

  Field field3;
  field3.resize(1);
  field3.grid = gridID3;

  int tsID = 0;
  while (true)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      const auto nrecs2 = cdo_stream_inq_timestep(streamID2, tsID);
      if (nrecs2 == 0) cdo_abort("Input streams have different number of timesteps!");

      cdo_taxis_copy_timestep(taxisID3, taxisID1);
      cdo_def_timestep(streamID3, tsID);

      for (int recID = 0; recID < nrecs; ++recID)
        {
          size_t nmiss;
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);
          cdo_read_record(streamID1, vars1[varID][levelID].vec_d.data(), &nmiss);
          if (nmiss) cdo_abort("Missing values unsupported for this operator!");

          cdo_inq_record(streamID2, &varID, &levelID);
          cdo_read_record(streamID2, vars2[varID][levelID].vec_d.data(), &nmiss);
          if (nmiss) cdo_abort("Missing values unsupported for this operator!");
        }

      for (int varID = 0; varID < nvars; ++varID)
        {
          auto wstatus = false;
          const auto gridID = vars1[varID][0].grid;
          if (needWeights && gridID != lastgrid)
            {
              lastgrid = gridID;
              wstatus = gridcell_weights(gridID, weights);
            }
          const auto code = vlistInqVarCode(vlistID1, varID);
          if (wstatus != 0 && tsID == 0 && code != oldcode) cdo_warning("Using constant area weights for code %d!", oldcode = code);

          field3.missval = vars1[varID][0].missval;
          varrms(weights, vars1[varID], vars2[varID], field3);

          cdo_def_record(streamID3, varID, 0);
          cdo_write_record(streamID3, field3.vec_d.data(), field3.nmiss);
        }

      tsID++;
    }

  cdo_stream_close(streamID3);
  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  vlistDestroy(vlistID3);

  cdo_finish();

  return nullptr;
}
