/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Vertint    zs2zl           Model depth level to depth level interpolation
*/

#include <cdi.h>

#include "cdo_options.h"
#include "process_int.h"
#include "cdo_vlist.h"
#include "field_vinterp.h"
#include "stdnametable.h"
#include "util_string.h"
#include "const.h"
#include "cdo_zaxis.h"
#include "param_conversion.h"

static bool
is_depth_axis(int zaxisID)
{
  return (zaxisInqType(zaxisID) == ZAXIS_DEPTH_BELOW_SEA);
}

static int
create_zaxis_depth(Varray<double> &depthLevels)
{
  int zaxisID = CDI_UNDEFID;
  const auto &arg1 = cdo_operator_argv(0);
  if (cdo_operator_argc() == 1 && !isdigit(arg1[0]))
    {
      auto zfilename = arg1.c_str();
      auto zfp = fopen(zfilename, "r");
      if (zfp)
        {
          zaxisID = zaxis_from_file(zfp, zfilename);
          fclose(zfp);
          if (zaxisID == CDI_UNDEFID) cdo_abort("Invalid zaxis description file %s!", zfilename);
          const auto nlevels = zaxisInqSize(zaxisID);
          depthLevels.resize(nlevels);
          zaxisInqLevels(zaxisID, depthLevels.data());
        }
      else if (arg1 == "default")
        depthLevels = { 1, 2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000 };
      else
        cdo_abort("Open failed on %s", zfilename);
    }
  else
    {
      depthLevels = cdo_argv_to_flt(cdo_get_oper_argv());
    }

  if (zaxisID == CDI_UNDEFID)
    {
      zaxisID = zaxisCreate(ZAXIS_DEPTH_BELOW_SEA, depthLevels.size());
      zaxisDefLevels(zaxisID, depthLevels.data());
    }

  return zaxisID;
}

void *
Vertintzs(void *process)
{
  auto extrapolate = true;  // do not use missing values

  cdo_initialize(process);

  cdo_operator_add("zs2zl", 0, 0, "depth levels in meter");

  const auto operatorID = cdo_operator_id();

  operator_input_arg(cdo_operator_enter(operatorID));

  Varray<double> depthLevels;
  const auto zaxisID2 = create_zaxis_depth(depthLevels);

  const auto streamID1 = cdo_open_read(0);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = vlistDuplicate(vlistID1);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = taxisDuplicate(taxisID1);
  vlistDefTaxis(vlistID2, taxisID2);

  const auto gridsize = vlist_check_gridsize(vlistID1);

  VarList varList1;
  varListInit(varList1, vlistID1);
  varListSetUniqueMemtype(varList1);
  const auto memType = varList1[0].memType;

  const auto nvars = vlistNvars(vlistID1);

  char varname[CDI_MAX_NAME];
  int depthID = -1;
  for (int varID = 0; varID < nvars; ++varID)
    {
      strcpy(varname, varList1[varID].name);
      cstr_to_lower_case(varname);

      // clang-format off
      if      (cdo_cmpstr(varname, "depth_c")) depthID = varID;
      // clang-format on
    }

  if (Options::cdoVerbose)
    {
      cdo_print("Found:");
      // clang-format off
      if (-1 != depthID)   cdo_print("  %s -> %s", "zstar depth at cell center", varList1[depthID].name);
      // clang-format on
    }

  if (-1 == depthID) cdo_abort("depth_c not found!");

  const auto zaxisIDfull = (-1 == depthID) ? -1 : varList1[depthID].zaxisID;
  const auto numFullLevels = (-1 == zaxisIDfull) ? 0 : zaxisInqSize(zaxisIDfull);

  const auto nzaxis = vlistNzaxis(vlistID1);
  for (int index = 0; index < nzaxis; ++index)
    {
      const auto zaxisID = vlistZaxis(vlistID1, index);
      const auto nlevels = zaxisInqSize(zaxisID);
      if (zaxisID == zaxisIDfull || (is_depth_axis(zaxisID) && nlevels == numFullLevels))
        vlistChangeZaxis(vlistID2, zaxisID, zaxisID2);
    }

  VarList varList2;
  varListInit(varList2, vlistID2);
  varListSetMemtype(varList2, memType);

  Field depthBottom;

  Varray<size_t> pnmiss;
  if (!extrapolate) pnmiss.resize(depthLevels.size());

  std::vector<int> vertIndexFull;
  vertIndexFull.resize(gridsize * depthLevels.size());

  std::vector<bool> vars(nvars), varinterp(nvars);
  Varray2D<size_t> varnmiss(nvars);
  Field3DVector vardata1(nvars), vardata2(nvars);

  const auto maxlev = std::max(numFullLevels, (int) depthLevels.size());

  for (int varID = 0; varID < nvars; ++varID)
    {
      const auto gridID = varList1[varID].gridID;
      const auto zaxisID = varList1[varID].zaxisID;
      const auto nlevels = varList1[varID].nlevels;

      if (gridInqType(gridID) == GRID_SPECTRAL) cdo_abort("Spectral data unsupported!");

      vardata1[varID].init(varList1[varID]);
      varnmiss[varID].resize(maxlev, 0);

      varinterp[varID] = (zaxisID == zaxisIDfull || (is_depth_axis(zaxisID) && (nlevels == numFullLevels)));

      if (varinterp[varID])
        {
          vardata2[varID].init(varList2[varID]);
        }
      else if (is_depth_axis(zaxisID) && nlevels > 1)
        {

          cdo_warning("Parameter %d has wrong number of levels, skipped! (name=%s nlevel=%d)", varID + 1, varList1[varID].name,
                      nlevels);
        }
    }

  Field3D fullDepth;
  fullDepth.init(varList1[depthID]);

  for (int varID = 0; varID < nvars; ++varID)
    {
      if (varinterp[varID] && varList1[varID].timetype == TIME_CONSTANT) vlistDefVarTimetype(vlistID2, varID, TIME_VARYING);
    }

  const auto streamID2 = cdo_open_write(1);

  cdo_def_vlist(streamID2, vlistID2);

  int tsID = 0;
  while (true)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      for (int varID = 0; varID < nvars; ++varID)
        {
          vars[varID] = false;
          const auto nlevels = varList1[varID].nlevels;
          for (int levelID = 0; levelID < nlevels; ++levelID) varnmiss[varID][levelID] = 0;
        }

      cdo_taxis_copy_timestep(taxisID2, taxisID1);
      cdo_def_timestep(streamID2, tsID);

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);
          cdo_read_record(streamID1, vardata1[varID], levelID, &varnmiss[varID][levelID]);
          vars[varID] = true;
        }

      for (int varID = 0; varID < nvars; ++varID)
        if (varinterp[varID]) vars[varID] = true;

      if (tsID == 0 || varList1[depthID].timetype != TIME_CONSTANT)
        {
          constexpr auto lreverse = false;
          field_copy(vardata1[depthID], fullDepth);
          gen_vert_index(vertIndexFull, depthLevels, fullDepth, gridsize, lreverse);
          if (!extrapolate)
            {
              depthBottom.init(varList1[depthID]);
              field_copy(fullDepth, numFullLevels - 1, depthBottom);
              gen_vert_index_mv(vertIndexFull, depthLevels, gridsize, depthBottom, pnmiss, lreverse);
            }
        }

      for (int varID = 0; varID < nvars; ++varID)
        {
          if (vars[varID])
            {
              if (tsID > 0 && !varinterp[varID] && varList1[varID].timetype == TIME_CONSTANT) continue;

              if (varinterp[varID])
                {
                  const auto nlevels = varList1[varID].nlevels;

                  if (nlevels != numFullLevels)
                    cdo_abort("Number of depth level differ from full level (param=%s)!", varList1[varID].name);

                  for (int levelID = 0; levelID < nlevels; ++levelID)
                    {
                      if (varnmiss[varID][levelID]) cdo_abort("Missing values unsupported for this operator!");
                    }

                  vertical_interp_X(fullDepth, vardata1[varID], vardata2[varID], vertIndexFull, depthLevels, gridsize);

                  if (!extrapolate) varray_copy(depthLevels.size(), pnmiss, varnmiss[varID]);
                }

              for (int levelID = 0; levelID < varList2[varID].nlevels; ++levelID)
                {
                  cdo_def_record(streamID2, varID, levelID);
                  auto varout = (varinterp[varID] ? vardata2[varID] : vardata1[varID]);
                  cdo_write_record(streamID2, varout, levelID, varnmiss[varID][levelID]);
                }
            }
        }

      tsID++;
    }

  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
