/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Yearmonstat   yearmonmean        Yearly mean from monthly data
      Yearmonstat   yearmonavg         Yearly average from monthly data
*/

#include "cdi.h"
#include "calendar.h"

#include "cdo_options.h"
#include "cdo_vlist.h"
#include "process_int.h"
#include "datetime.h"
#include "printinfo.h"


void *
Yearmonstat(void *process)
{
  TimeStat timestat_date = TimeStat::MEAN;
  CdiDateTime vDateTime0 = { };
  int year0 = 0, month0 = 0;
  int year, month, day;

  cdo_initialize(process);

  // clang-format off
  cdo_operator_add("yearmonmean",  FieldFunc_Mean, 0, nullptr);
  cdo_operator_add("yearmonavg",   FieldFunc_Avg,  0, nullptr);
  // clang-format on

  const auto operatorID = cdo_operator_id();
  const auto operfunc = cdo_operator_f1(operatorID);

  operator_check_argc(0);

  const auto streamID1 = cdo_open_read(0);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = vlistDuplicate(vlistID1);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = taxisDuplicate(taxisID1);
  taxisWithBounds(taxisID2);
  vlistDefTaxis(vlistID2, taxisID2);

  const auto streamID2 = cdo_open_write(1);
  cdo_def_vlist(streamID2, vlistID2);

  const auto nvars = vlistNvars(vlistID1);

  const auto maxrecs = vlistNrecs(vlistID1);
  std::vector<RecordInfo> recList(maxrecs);

  const auto calendar = taxisInqCalendar(taxisID1);
  DateTimeList dtlist;
  dtlist.set_stat(timestat_date);
  dtlist.set_calendar(calendar);

  VarList varList;
  varListInit(varList, vlistID1);

  Field field;

  FieldVector2D samp1, vars1;
  fields_from_vlist(vlistID1, samp1);
  fields_from_vlist(vlistID1, vars1, FIELD_VEC);

  int tsID = 0;
  int otsID = 0;
  while (true)
    {
      int nrecs = 0;
      long nsets = 0;
      double dsets = 0.0;
      while (true)
        {
          nrecs = cdo_stream_inq_timestep(streamID1, tsID);
          if (nrecs == 0) break;

          dtlist.taxis_inq_timestep(taxisID1, nsets);
          const auto vDateTime = dtlist.get_vDateTime(nsets);
          cdiDate_decode(vDateTime.date, &year, &month, &day);

          if (nsets == 0) year0 = year;

          if (year != year0)
            {
              cdo_add_steps(-1);
              break;
            }

          if (nsets > 0 && month == month0)
            {
              cdo_warning("   last timestep: %s", datetime_to_string(vDateTime0));
              cdo_warning("current timestep: %s", datetime_to_string(vDateTime));
              cdo_abort("Month does not change!");
            }

          const auto dpm = days_per_month(calendar, year, month);

          for (int recID = 0; recID < nrecs; ++recID)
            {
              int varID, levelID;
              cdo_inq_record(streamID1, &varID, &levelID);

              if (tsID == 0)
                {
                  recList[recID].varID = varID;
                  recList[recID].levelID = levelID;
                  recList[recID].lconst = (varList[varID].timetype == TIME_CONSTANT);
                }

              auto &rsamp1 = samp1[varID][levelID];
              auto &rvars1 = vars1[varID][levelID];

              if (nsets == 0)
                {
                  cdo_read_record(streamID1, rvars1);

                  fieldc_mul(rvars1, dpm);

                  if (rvars1.nmiss || !rsamp1.empty())
                    {
                      if (rsamp1.empty()) rsamp1.resize(rvars1.size);
                      field2_vinit(rsamp1, rvars1, dpm);
                    }
                }
              else
                {
                  field.init(varList[varID]);
                  cdo_read_record(streamID1, field);

                  fieldc_mul(field, dpm);

                  if (field.nmiss || !rsamp1.empty())
                    {
                      if (rsamp1.empty()) rsamp1.resize(rvars1.size, dsets);
                      field2_vincr(rsamp1, field, dpm);
                    }

                  field2_function(rvars1, field, operfunc);
                }
            }

          month0 = month;
          vDateTime0 = vDateTime;
          nsets++;
          dsets += dpm;
          tsID++;
        }

      if (nrecs == 0 && nsets == 0) break;

      for (int varID = 0; varID < nvars; ++varID)
        {
          if (varList[varID].timetype == TIME_CONSTANT) continue;
          for (int levelID = 0; levelID < varList[varID].nlevels; ++levelID)
            {
              const auto &rsamp1 = samp1[varID][levelID];
              auto &rvars1 = vars1[varID][levelID];
              if (!rsamp1.empty())
                field2_div(rvars1, rsamp1);
              else
                fieldc_div(rvars1, dsets);
            }
        }

      if (Options::cdoVerbose)
        cdo_print("%s  nsets = %ld", datetime_to_string(vDateTime0), nsets);

      dtlist.stat_taxis_def_timestep(taxisID2, nsets);
      cdo_def_timestep(streamID2, otsID);

      for (int recID = 0; recID < maxrecs; ++recID)
        {
          if (otsID && recList[recID].lconst) continue;

          const auto varID = recList[recID].varID;
          const auto levelID = recList[recID].levelID;
          auto &rvars1 = vars1[varID][levelID];
          cdo_def_record(streamID2, varID, levelID);
          cdo_write_record(streamID2, rvars1);
        }

      if (nrecs == 0) break;
      otsID++;
    }

  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
