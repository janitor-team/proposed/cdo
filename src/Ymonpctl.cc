/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Copyright (C) 2006 Brockmann Consult

  Author: Ralf Quast
          Uwe Schulzweida

*/

/*
   This module contains the following operators:

      Ymonpctl   ymonpctl        Multi-year monthly percentiles
*/

#include <cdi.h>

#include "cdo_options.h"
#include "cdo_vlist.h"
#include "datetime.h"
#include "process_int.h"
#include "param_conversion.h"
#include "percentiles_hist.h"

void *
Ymonpctl(void *process)
{
  constexpr int MaxMonths = 17;
  CdiDateTime vDateTimes1[MaxMonths] = { };
  CdiDateTime vDateTimes2[MaxMonths] = { };
  long nsets[MaxMonths] = { 0 };
  std::vector<bool> vars1(MaxMonths, false);
  HistogramSet hsets[MaxMonths];

  cdo_initialize(process);
  cdo_operator_add("ymonpctl", FieldFunc_Pctl, 0, nullptr);

  operator_input_arg("percentile number");
  const auto pn = parameter_to_double(cdo_operator_argv(0));

  const auto streamID1 = cdo_open_read(0);
  const auto streamID2 = cdo_open_read(1);
  const auto streamID3 = cdo_open_read(2);

  const auto vlistID1 = cdo_stream_inq_vlist(streamID1);
  const auto vlistID2 = cdo_stream_inq_vlist(streamID2);
  const auto vlistID3 = cdo_stream_inq_vlist(streamID3);
  const auto vlistID4 = vlistDuplicate(vlistID1);

  vlist_compare(vlistID1, vlistID2, CMP_ALL);
  vlist_compare(vlistID1, vlistID3, CMP_ALL);

  const auto taxisID1 = vlistInqTaxis(vlistID1);
  const auto taxisID2 = vlistInqTaxis(vlistID2);
  const auto taxisID3 = vlistInqTaxis(vlistID3);
  // TODO - check that time axes 2 and 3 are equal

  const auto taxisID4 = taxisDuplicate(taxisID1);
  if (taxisHasBounds(taxisID4)) taxisDeleteBounds(taxisID4);
  vlistDefTaxis(vlistID4, taxisID4);

  const auto streamID4 = cdo_open_write(3);
  cdo_def_vlist(streamID4, vlistID4);

  const auto ntsteps = vlistNtsteps(vlistID1);
  const auto nvars = vlistNvars(vlistID1);

  const auto maxrecs = vlistNrecs(vlistID1);
  std::vector<RecordInfo> recList(maxrecs);

  FieldVector constFields(maxrecs);

  Field field1, field2;

  VarList varList1;
  varListInit(varList1, vlistID1);

  int tsID = 0;
  while (true)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID2, tsID);
      if (nrecs == 0) break;

      if (nrecs != cdo_stream_inq_timestep(streamID3, tsID))
        cdo_abort("Number of records at time step %d of %s and %s differ!", tsID + 1, cdo_get_stream_name(1),
                  cdo_get_stream_name(2));

      const auto vDateTime2 = taxisInqVdatetime(taxisID2);
      const auto vDateTime3 = taxisInqVdatetime(taxisID3);

      if (cdiDate_get(vDateTime2.date) != cdiDate_get(vDateTime3.date))
        cdo_abort("Verification dates at time step %d of %s and %s differ!", tsID + 1, cdo_get_stream_name(1),
                  cdo_get_stream_name(2));

      // if (Options::cdoVerbose) cdo_print("process timestep: %d %s", tsID + 1, datetime_to_string(vDateTime2));

      const auto month = decode_month(vDateTime2.date);
      if (month < 0 || month >= MaxMonths) cdo_abort("Month %d out of range!", month);

      vDateTimes2[month] = vDateTime2;

      if (!vars1[month])
        {
          vars1[month] = true;
          hsets[month].create(nvars, ntsteps);
          for (int varID = 0; varID < nvars; ++varID)
            hsets[month].createVarLevels(varID, varList1[varID].nlevels, varList1[varID].gridsize);
        }

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID2, &varID, &levelID);
          field1.init(varList1[varID]);
          cdo_read_record(streamID2, field1);

          cdo_inq_record(streamID3, &varID, &levelID);
          field2.init(varList1[varID]);
          cdo_read_record(streamID3, field2);

          hsets[month].defVarLevelBounds(varID, levelID, field1, field2);
        }

      tsID++;
    }

  tsID = 0;
  while (true)
    {
      const auto nrecs = cdo_stream_inq_timestep(streamID1, tsID);
      if (nrecs == 0) break;

      const auto vDateTime = taxisInqVdatetime(taxisID1);

      // if (Options::cdoVerbose) cdo_print("process timestep: %d %s", tsID + 1, datetime_to_string(vDateTime));

      const auto month = decode_month(vDateTime.date);
      if (month < 0 || month >= MaxMonths) cdo_abort("Month %d out of range!", month);

      vDateTimes1[month] = vDateTime;

      if (!vars1[month]) cdo_abort("No data for month %d in %s and %s", month, cdo_get_stream_name(1), cdo_get_stream_name(2));

      for (int recID = 0; recID < nrecs; ++recID)
        {
          int varID, levelID;
          cdo_inq_record(streamID1, &varID, &levelID);

          if (tsID == 0)
            {
              recList[recID].varID = varID;
              recList[recID].levelID = levelID;
              recList[recID].lconst = (varList1[varID].timetype == TIME_CONSTANT);
            }

          if (tsID == 0 && recList[recID].lconst)
            {
              constFields[recID].init(varList1[varID]);
              cdo_read_record(streamID1, constFields[recID]);
            }
          else
            {
              field1.init(varList1[varID]);
              cdo_read_record(streamID1, field1);

              hsets[month].addVarLevelValues(varID, levelID, field1);
            }
        }

      nsets[month]++;
      tsID++;
    }

  int otsID = 0;
  for (int month = 0; month < MaxMonths; ++month)
    if (nsets[month])
      {
        if (decode_month(vDateTimes1[month].date) != decode_month(vDateTimes2[month].date))
          cdo_abort("Verification dates for the month %d of %s and %s are different!", month, cdo_get_stream_name(0),
                    cdo_get_stream_name(1));

        taxisDefVdatetime(taxisID4, vDateTimes1[month]);
        cdo_def_timestep(streamID4, otsID);

        for (int recID = 0; recID < maxrecs; ++recID)
          {
            if (otsID && recList[recID].lconst) continue;

            const auto varID = recList[recID].varID;
            const auto levelID = recList[recID].levelID;
            cdo_def_record(streamID4, varID, levelID);

            if (recList[recID].lconst)
              {
                cdo_write_record(streamID4, constFields[recID]);
              }
            else
              {
                field1.init(varList1[varID]);
                hsets[month].getVarLevelPercentiles(field1, varID, levelID, pn);
                cdo_write_record(streamID4, field1);
              }
          }

        otsID++;
      }

  cdo_stream_close(streamID4);
  cdo_stream_close(streamID3);
  cdo_stream_close(streamID2);
  cdo_stream_close(streamID1);

  cdo_finish();

  return nullptr;
}
