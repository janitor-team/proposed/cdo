#ifndef CDO_CDI_WRAPPER_H
#define CDO_CDI_WRAPPER_H

// convert a CDI filetype to string
const char *cdi_filetype_to_str(int filetype);

// convert a CDI datatype to string
const char *cdi_datatype_to_str(int datatype);
int cdo_str_to_datatype(const char *datatypestr);

// create Taxis(cdi) with added check/setting of taxisType
int cdo_taxis_create(int taxisType);
void cdo_taxis_copy_timestep(int taxisIDdes, int taxisIDsrc);

// cdi
void grid_gen_xvals(int xsize, double xfirst, double xlast, double xinc, double *xvals);
void grid_gen_yvals(int gridtype, int ysize, double yfirst, double ylast, double yinc, double *yvals);

void cdo_def_table_id(int tableID);
#endif
