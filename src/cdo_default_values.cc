#include <cdi.h>
#include "cdo_default_values.h"

namespace CdoDefault
{
int FileType = CDI_UNDEFID;
int DataType = CDI_UNDEFID;
int Byteorder = CDI_UNDEFID;
int TableID = CDI_UNDEFID;
int InstID = CDI_UNDEFID;
int TaxisType = CDI_UNDEFID;
}  // namespace CdoDefault
