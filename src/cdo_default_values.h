#ifndef CDO_DEFAULT_VALUES_H
#define CDO_DEFAULT_VALUES_H

#include "cdi.h"

namespace CdoDefault
{
extern int FileType;
extern int DataType;
extern int Byteorder;
extern int TableID;
extern int InstID;
extern int TaxisType;
}  // namespace CdoDefault

#endif
