#ifndef CDO_FEATURES_H
#define CDO_FEATURES_H

int cdo_print_config(const std::string &option);
void cdo_print_features(void);
void cdo_print_libraries(void);

#endif
