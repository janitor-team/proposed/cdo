#ifndef CDO_MATH_H
#define CDO_MATH_H

// clang-format off
namespace cdo
{
  constexpr double sqr(const double a) noexcept { return a * a; }
}
// clang-format on

#endif
