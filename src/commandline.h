#ifndef COMMANDLINE_H
#define COMMANDLINE_H

void set_command_line(int argc, char **argv);
const char *command_line(void);

#endif
