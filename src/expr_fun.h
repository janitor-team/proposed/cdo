/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/
#ifndef EXPR_FUN_H
#define EXPR_FUN_H

#include <cstddef>
#include "field.h"

nodeType *
expr_con_con(const int oper, const nodeType *p1, const nodeType *p2);
void
oper_expr_con_var(const int oper, const bool hasMV, const size_t n, const double mv, double *odat,
                  const double cval, const double *idat);
void
oper_expr_var_con(const int oper, const bool hasMV, const size_t n, const double mv,
                  double *odat, const double *idat, const double cval);
void
oper_expr_var_var(const int oper, const bool hasMV, const size_t n, const double mv1, const double mv2,
                  double *odat, const double *idat1, const double *idat2);

void fld_field_init(Field &field, size_t nmiss, double missval, size_t ngp, double *array, double *w);
void vert_weights(int zaxisID, size_t nlev, Varray<double> &weights);

#endif
