/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/
#ifndef FIELD_H
#define FIELD_H

#include <cstdio>
#include "varray.h"
#include "cdo_options.h"
#include "cdo_vlist.h"
#include "compare.h"

// clang-format off
const auto memtype_is_float_float   = [](auto a, auto b) noexcept { return (a == MemType::Float  && b == MemType::Float); };
const auto memtype_is_double_float  = [](auto a, auto b) noexcept { return (a == MemType::Double && b == MemType::Float); };
const auto memtype_is_float_double  = [](auto a, auto b) noexcept { return (a == MemType::Float  && b == MemType::Double); };
const auto memtype_is_double_double = [](auto a, auto b) noexcept { return (a == MemType::Double && b == MemType::Double); };
// clang-format on

enum FieldFunc
{
  FieldFunc_Min = 100,
  FieldFunc_Max,
  FieldFunc_Range,
  FieldFunc_Sum,
  FieldFunc_Avg,
  FieldFunc_Mean,
  FieldFunc_Var,
  FieldFunc_Var1,
  FieldFunc_Std,
  FieldFunc_Std1,
  FieldFunc_Skew,
  FieldFunc_Kurt,
  FieldFunc_Median,
  FieldFunc_Pctl,

  FieldFunc_Cor,
  FieldFunc_Covar,
  FieldFunc_Avgw,
  FieldFunc_Meanw,
  FieldFunc_Stdw,
  FieldFunc_Std1w,
  FieldFunc_Varw,
  FieldFunc_Var1w,
  FieldFunc_Minidx,
  FieldFunc_Maxidx,
  FieldFunc_Rmsd,

  FieldFunc_Add,
  FieldFunc_Sub,
  FieldFunc_Mul,
  FieldFunc_Div,
  FieldFunc_Mod,

  FieldFunc_Atan2,
  FieldFunc_Setmiss,
};

double var_to_std(double rvar, double missval);

enum field_flag
{
  FIELD_VEC = 2,  // allocated memory
  FIELD_FLT = 4,  // 32-bit float
  FIELD_DBL = 8,  // 64-bit float
  FIELD_NAT = 16, // native: 32-bit float for 32-bit float data, otherweise 64-bit float
};

class // Field
#ifdef WARN_UNUSED
[[gnu::warn_unused]]
#endif
Field
{
public:
  int fpeRaised = 0;
  int nwpv = 1;         // number of words per value; real:1  complex:2
  int grid = -1;
  MemType memType = MemType::Native;  // MemType::Float or MemType::Double

  size_t gridsize = 0;
  size_t size = 0;
  size_t nsamp = 0;

  size_t nmiss = 0;
  double missval = 0;

  Varray<float> vec_f;
  Varray<double> vec_d;
  Varray<double> weightv;

  Field() {}
  void init(const CdoVar &var);
  void resize(size_t count);
  void resize(size_t count, double value);
  void resizef(size_t count);
  void resizef(size_t count, float value);
  bool empty() const;
  void check_gridsize() const;

  bool hasData() const
  {
    return (memType == MemType::Float) ? !vec_f.empty() : !vec_d.empty();
  }

private:
  size_t m_count = 0;
};

class // Field3D
#ifdef WARN_UNUSED
[[gnu::warn_unused]]
#endif
Field3D : public Field
{
public:
  size_t nlevels = 0;

  Field3D() {}
  void init(const CdoVar &var);
};

struct RecordInfo
{
  int varID = 0;
  int levelID = 0;
  bool lconst = false;
};

using FieldVector = std::vector<Field>;
using FieldVector2D = std::vector<FieldVector>;
using FieldVector3D = std::vector<FieldVector2D>;

using Field3DVector = std::vector<Field3D>;

void field_fill(Field &field, double value);
void field_ncopy(size_t n, const Field &fieldIn, Field &fieldOut);
void field_copy(const Field &fieldIn, Field &fieldOut);
void field_copy(const Field3D &fieldIn, Field3D &fieldOut);
void field_copy(const Field3D &fieldIn, int levelID, Field &fieldOut);
void field_add(Field &field1, const Field3D &field2, int levelID);
size_t field_num_miss(const Field &field);
size_t field_num_mv(Field &field);
MinMax field_min_max(const Field &field);

template <class UnaryOperation>
void field_transform(const Field &fieldIn, Field &fieldOut, UnaryOperation unary_op)
{
  if (fieldIn.memType == MemType::Float && fieldOut.memType == MemType::Float)
    varray_transform(fieldIn.vec_f, fieldOut.vec_f, unary_op);
  else
    varray_transform(fieldIn.vec_d, fieldOut.vec_d, unary_op);
}

// fieldmem.cc
void fields_from_vlist(int vlistID, FieldVector2D &field2D);
void fields_from_vlist(int vlistID, FieldVector2D &field2D, int ptype);
void fields_from_vlist(int vlistID, FieldVector2D &field2D, int ptype, double fillValue);

// field.cc
double field_function(const Field &field, int function);
double field_min(const Field &field);
double field_range(const Field &field);
double field_max(const Field &field);
double field_sum(const Field &field);
double field_mean(const Field &field);
double field_meanw(const Field &field);
double field_avg(const Field &field);
double field_avgw(const Field &field);
double field_std(const Field &field);
double field_std1(const Field &field);
double field_var(const Field &field);
double field_var1(const Field &field);
double field_stdw(const Field &field);
double field_std1w(const Field &field);
double field_varw(const Field &field);
double field_var1w(const Field &field);
double field_skew(const Field &field);
double field_kurt(const Field &field);
double field_median(const Field &field);

// ENS validation
double field_rank(Field &field);

double field_pctl(Field &field, double pn);

// field_zonal.cc
void zonal_function(const Field &field1, Field &field2, int function);
void zonal_min(const Field &field1, Field &field2);
void zonal_max(const Field &field1, Field &field2);
void zonal_range(const Field &field1, Field &field2);
void zonal_sum(const Field &field1, Field &field2);
void zonal_avg(const Field &field1, Field &field2);
void zonal_mean(const Field &field1, Field &field2);
void zonal_std(const Field &field1, Field &field2);
void zonal_std1(const Field &field1, Field &field2);
void zonal_var(const Field &field1, Field &field2);
void zonal_var1(const Field &field1, Field &field2);
void zonal_skew(const Field &field1, Field &field2);
void zonal_kurt(const Field &field1, Field &field2);
void zonal_median(const Field &field1, Field &field2);
void zonal_pctl(const Field &field1, Field &field2, double pn);

// field_meridional.cc
void meridional_function(const Field &field1, Field &field2, int function);
void meridional_pctl(const Field &field1, Field &field2, double pn);

void field_rms(const Field &field1, const Field &field2, Field &field3);

// fieldc.cc
void fieldc_function(Field &field, double rconst, int function);
void fieldc_mul(Field &field, double rconst);
void fieldc_div(Field &field, double rconst);
void fieldc_add(Field &field, double rconst);
void fieldc_sub(Field &field, double rconst);
void fieldc_min(Field &field, double rconst);
void fieldc_max(Field &field, double rconst);
void fieldc_mod(Field &field, double divisor);

// fieldc_complex.cc
void fieldc_function_complex(Field &field, const double rconstcplx[2], int function);

// field2.cc
void field2_function(Field &field1, const Field &field2, int function);

void field2_add(Field &field1, const Field &field2);
void field2_sum(Field &field1, const Field &field2);
void field2_sub(Field &field1, const Field &field2);
void field2_mul(Field &field1, const Field &field2);
void field2_div(Field &field1, const Field &field2);
void field2_min(Field &field1, const Field &field2);
void field2_max(Field &field1, const Field &field2);
void field2_atan2(Field &field1, const Field &field2);

void field2_sumq(Field &field1, const Field &field2);
void field2_sumw(Field &field1, const Field &field2, double w);
void field2_sumqw(Field &field1, const Field &field2, double w);
void field2_sumtr(Field &field1, const Field &field2, double refval);
void field2_vinit(Field &field1, const Field &field2);
void field2_vincr(Field &field1, const Field &field2);
void field2_vinit(Field &field1, const Field &field2, int vinit);
void field2_vincr(Field &field1, const Field &field2, int vincr);

void field2_sumsumq(Field &field1, Field &field2, const Field &field3);
void field2_maxmin(Field &field1, Field &field2, const Field &field3);
void field2_minidx(Field &field1, Field &field2, const Field &field3, int idx);
void field2_maxidx(Field &field1, Field &field2, const Field &field3, int idx);
void field2_var(Field &field1, const Field &field2, const Field &field3, int divisor);
void field2_std(Field &field1, const Field &field2, const Field &field3, int divisor);
void fieldc_var(Field &field1, const Field &field2, int nsets, int divisor);
void fieldc_std(Field &field1, const Field &field2, int nsets, int divisor);
void field2_moq(Field &field1, const Field &field2);
void field2_moqw(Field &field1, const Field &field2, double w);

void field2_count(Field &field1, const Field &field2);

// field2_complex.cc
void field2_function_complex(Field &field1, const Field &field2, int function);

#endif /* FIELD_H */
