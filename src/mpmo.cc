#include "mpmo.h"

namespace MpMO
{

bool silentMode = false;
bool warningsEnabled = true;
bool verbose = false;
bool pedantic = false;
bool exitOnError = true;
int DebugLevel = 0;

void
Debug_(const char *p_func, const char *context, int p_debugScope, std::function<void()> p_function)
{
  (void) p_func;
  if (p_debugScope) p_function();
}

void
Debug_(const char *p_func, const char *context, std::function<void()> p_function)
{
  (void) p_func;
  if (DebugLevel > 0) p_function();
}

void
Verbose_(bool p_verbose, std::function<void()> p_function) noexcept
{
  if (p_verbose) p_function();
}

void
enable_silent_mode(bool enable)
{
  silentMode = enable;
}

void
enable_warnings(bool enable)
{
  warningsEnabled = enable;
}

void
enable_pedantic(bool enable)
{
  pedantic = enable;
}

void
enable_verbose(bool enable)
{
  verbose = enable;
}
}  // namespace MpMO

std::string
argv_to_string(int argc, const char **argv)
{
  std::string input_string = "";
  for (int i = 0; i < argc; ++i)
    {
      input_string += argv[i];
      input_string += " ";
    }
  return input_string;
}

std::string
argv_to_string(std::vector<std::string> argv)
{
  std::string input_string = "";
  int argc = (int) argv.size();
  for (int i = 0; i < argc; ++i)
    {
      input_string += argv[i];
      input_string += " ";
    }
  return input_string;
}
