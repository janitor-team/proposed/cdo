#ifndef PARSER_H
#define PARSER_H

#include <vector>
#include "node.h"

namespace Parser
{
std::vector<std::shared_ptr<Node>> parse(const std::vector<std::string> argv);
}  // namespace Parser

#endif
