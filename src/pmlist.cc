/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida

*/

#include <cstring>
#include <cstdlib>
#include <vector>

#include "pmlist.h"
#include "namelist.h"
#include "cdo_output.h"

static void
keyValuesPrint(FILE *fp, const KeyValues &keyValues)
{
  fprintf(fp, "  %s =", keyValues.key.c_str());
  for (int i = 0; i < keyValues.nvalues; ++i) fprintf(fp, " '%s'", keyValues.values[i].c_str());
  fprintf(fp, "\n");
}

void
KVList::print(FILE *fp) const
{
  for (const auto &keyValues : *this) keyValuesPrint(fp, keyValues);
}

int
KVList::parse_arguments(const int argc, const std::vector<std::string> &argv)
{
  // Assume key = value pairs. That is, if argv[i] contains no '=' then treat it as if it belongs to the values of argv[i-1].
  char key[256];
  int i = 0;
  while (i < argc)
    {
      const char *currentArgv = argv[i].c_str();
      const char *end = strchr(currentArgv, '=');
      if (end == nullptr)
        {
          fprintf(stderr, "Missing '=' in key/value string: >%s<\n", currentArgv);
          return -1;
        }

      snprintf(key, sizeof(key), "%.*s", (int) (end - currentArgv), currentArgv);
      key[sizeof(key) - 1] = 0;

      int j = 1;
      while (i + j < argc && strchr(argv[i + j].c_str(), '=') == nullptr) j++;

      int nvalues = j;

      KeyValues kv;
      kv.values.resize(1);
      kv.values[0] = end + 1;
      if (kv.values[0][0] == 0) nvalues = 0;

      kv.key = key;
      kv.nvalues = nvalues;
      kv.values.resize(nvalues);

      for (j = 1; j < nvalues; ++j) kv.values[j] = argv[i + j];
      this->push_back(kv);

      i += j;
    }

  return 0;
}

const KeyValues *
KVList::search(const std::string &key) const
{
  for (const auto &kv : *this)
    {
      if (kv.key == key) return &kv;
    }

  return nullptr;
}

char *
KVList::get_first_value(const char *key, const char *replacer)
{
  auto kv = this->search(key);
  if (kv && kv->nvalues > 0) return (char *) kv->values[0].c_str();
  if (replacer)
    return (char *) replacer;
  else
    return nullptr;
}

void
KVList::append(const char *key, const char *const *values, int nvalues)
{
  KeyValues kv;
  kv.key = strdup(key);
  kv.nvalues = nvalues;
  kv.values.resize(nvalues);
  for (int i = 0; i < nvalues; ++i) kv.values[i] = strdup(values[i]);
  this->push_back(kv);
}

// Remove only one list item
void
KVList::remove(const std::string &inkey)
{
  std::list<KeyValues>::iterator i;
  for (i = this->begin(); i != this->end(); ++i)
    if (i->key == inkey) break;
  if (i->key == inkey) this->erase(i);
}

const KVList *
PMList::searchKVListVentry(const std::string &key, const std::string &value, const std::vector<std::string> &entry)
{
  for (const auto &kvlist : *this)
    {
      for (const auto &s : entry)
        if (kvlist.name == s)
          {
            auto kv = kvlist.search(key);
            if (kv && kv->nvalues > 0 && kv->values[0] == value) return &kvlist;
          }
    }

  return nullptr;
}

const KVList *
PMList::getKVListVentry(const std::vector<std::string> &entry)
{
  for (const auto &kvlist : *this)
    {
      for (const auto &s : entry)
        if (kvlist.name == s) return &kvlist;
    }

  return nullptr;
}

static void
KVListAppendNamelist(KVList &kvlist, const char *key, const char *buffer, NamelistToken *t, int nvalues, bool cdocmor)
{
  std::vector<char> value;
  KeyValues kv;
  kv.key = key;
  kv.nvalues = nvalues;
  if (nvalues > 0) kv.values.resize(nvalues);

  for (int i = 0; i < nvalues; ++i)
    {
      const size_t len = t[i].end - t[i].start;
      /** CMOR_MAX_STRING cannot be used **/
      if (cdocmor && len > 1024) cdo_abort("A string value is larger than the maximum size allowed by CMOR (1024 signs).");

      if (value.size() < (len + 1)) value.resize(len + 1);
      const auto pval = buffer + t[i].start;

      if (cdocmor && strcmp(key, "code") == 0 && !strstr(pval, ","))
        {
          value.resize(4);
          const auto code = atol(pval);
          if (code > 0 && code < 1000)
            snprintf(value.data(), 4, "%03ld", code);
          else
            cdo_warning("In parsing a line of a file:\n          "
                        "Codes could not be transformed into the code format (three digit integer). Codes wont be used.");
        }
      else
        {
          memcpy(value.data(), pval, len);
          value[len] = 0;
        }

      kv.values[i] = value.data();
    }

  kvlist.push_back(std::move(kv));
}

static unsigned long
get_number_of_values(const unsigned long ntok, const NamelistToken *tokens)
{
  unsigned long it;

  for (it = 0; it < ntok; ++it)
    {
      const auto type = tokens[it].type;
      if (type != NamelistType::WORD && type != NamelistType::STRING) break;
    }

  return it;
}

static void
replace_name(char *name)
{
  for (size_t pos = 0; pos < strlen(name); pos++) name[pos] = tolower(name[pos]);

  if (strcmp(name, "conventions") == 0) strcpy(name, "Conventions");
  if (strcmp(name, "cn") == 0) strcpy(name, "cmor_name");
  if (strcmp(name, "c") == 0) strcpy(name, "code");
  if (strcmp(name, "n") == 0) strcpy(name, "name");
  if (strcmp(name, "pmt") == 0) strcpy(name, "project_mip_table");
  if (strcmp(name, "cordex_domain") == 0) strcpy(name, "CORDEX_domain");
  if (strcmp(name, "char_axis_landuse") == 0) strcpy(name, "char_axis_landUse");
  if (strcmp(name, "_formula_var_file") == 0) strcpy(name, "_FORMULA_VAR_FILE");
  if (strcmp(name, "_axis_entry_file") == 0) strcpy(name, "_AXIS_ENTRY_FILE");
}

int
parse_namelist(PMList &pmlist, NamelistParser &parser, char *buf, bool cdocmor)
{
  char name[4096];
  KVList kvlist;
  auto &tokens = parser.tokens;
  const auto ntok = parser.toknext;

  for (unsigned long it = 0; it < ntok; ++it)
    {
      const auto &t = tokens[it];
      // printf("Token %u", it+1);
      if (t.type == NamelistType::OBJECT)
        {
          name[0] = 0;
          if (it + 1 < ntok && tokens[it + 1].type == NamelistType::WORD)
            {
              it++;
              const auto &t2 = tokens[it];
              snprintf(name, sizeof(name), "%.*s", (int)(t2.end - t2.start), buf + t2.start);
              name[sizeof(name) - 1] = 0;
            }

          if (kvlist.size())
            {
              pmlist.push_back(kvlist);
              kvlist.clear();
            }

          kvlist.name = name;
        }
      else if (t.type == NamelistType::KEY)
        {
          // printf(" key >%.*s<\n", t.end - t.start, buf+t.start);
          snprintf(name, sizeof(name), "%.*s", (int)(t.end - t.start), buf + t.start);
          name[sizeof(name) - 1] = 0;
          const auto nvalues = get_number_of_values(ntok - it - 1, &tokens[it + 1]);

          if (cdocmor)
            {
              if (nvalues == 0)
                {
                  cdo_warning("Could not find values for key '%s'.", name);
                  continue;
                }
              replace_name(name);
            }

          KVListAppendNamelist(kvlist, name, buf, &tokens[it + 1], nvalues, cdocmor);
          it += nvalues;
        }
      else
        {
          // printf(" token >%.*s<\n", (int)(t.end - t.start), buf+t.start);
          break;
        }
    }

  if (kvlist.size()) pmlist.push_back(kvlist);

  return 0;
}

int
parse_list_buffer(NamelistParser &p, ListBuffer &listBuffer)
{
  const char *errMsg = "Namelist error";
  const auto name = listBuffer.name.c_str();

  const auto status = p.parse(listBuffer.buffer.data(), listBuffer.buffer.size());
  if (status != NamelistError::UNDEFINED)
    {
      switch (status)
        {
        case NamelistError::INVAL:
          fprintf(stderr, "%s: Invalid character in %s (line=%lu character='%c' dec=%u)!\n", errMsg, name, p.lineno,
                  listBuffer.buffer[p.pos], (unsigned char) listBuffer.buffer[p.pos]);
          break;
        case NamelistError::PART: fprintf(stderr, "%s: End of string not found in %s (line=%lu)!\n", errMsg, name, p.lineno); break;
        case NamelistError::INKEY: fprintf(stderr, "%s: Invalid keyword in %s (line=%lu)!\n", errMsg, name, p.lineno); break;
        case NamelistError::INTYP: fprintf(stderr, "%s: Invalid keyword type in %s (line=%lu)!\n", errMsg, name, p.lineno); break;
        case NamelistError::INOBJ: fprintf(stderr, "%s: Invalid object in %s (line=%lu)!\n", errMsg, name, p.lineno); break;
        case NamelistError::EMKEY: fprintf(stderr, "%s: Empty key name in %s (line=%lu)!\n", errMsg, name, p.lineno); break;
        default: fprintf(stderr, "%s in %s (line=%lu)!\n", errMsg, name, p.lineno); break;
        }
      cdo_abort("%s!", errMsg);
    }

  // p.dump(listBuffer.buffer.data());
  if (p.verify())
    {
      fprintf(stderr, "%s: Invalid contents in %s!\n", errMsg, name);
      cdo_abort("Namelist error!");
    }

  return 0;
}

void
PMList::read_namelist(FILE *fp, const char *name)
{
  ListBuffer listBuffer;
  if (listBuffer.read(fp, name)) cdo_abort("Read error on namelist %s!", name);

  NamelistParser p;
  const auto status = parse_list_buffer(p, listBuffer);
  if (status) cdo_abort("Namelist not found!");

  parse_namelist(*this, p, listBuffer.buffer.data(), false);
}

void
PMList::print(FILE *fp)
{
  for (const auto &kvlist : *this)
    {
      const auto listname = kvlist.name.c_str();
      fprintf(fp, "\nFound %s list with %zu key/values: \n", listname ? listname : "", kvlist.size());
      kvlist.print(fp);
    }
}
