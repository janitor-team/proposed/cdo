#ifndef PRINTINFO_H
#define PRINTINFO_H

#include <stdio.h>
#include <inttypes.h>

#include "process_int.h"

std::string date_to_string(CdiDate date);
std::string time_to_string(CdiTime time);
std::string datetime_to_string(CdiDateTime dt);

const char *comptype_to_name(int comptype);

void print_filetype(CdoStreamID p_streamID, int vlistID);
void print_grid_info(int vlistID);
void print_zaxis_info(int vlistID);
void print_subtype_info(int vlistID);
void print_timesteps(CdoStreamID streamID, int taxisID, int verbose);

#endif
/*
 * Local Variables:
 * c-file-style: "Java"
 * c-basic-offset: 2
 * indent-tabs-mode: nil
 * show-trailing-whitespace: t
 * require-trailing-newline: t
 * End:
 */
