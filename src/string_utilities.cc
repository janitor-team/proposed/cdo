/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida
          Oliver Heidmann

*/

#include <algorithm>

#include "cdo_output.h"
#include "string_utilities.h"
#include "util_string.h"

#define DBG 0

std::vector<std::string>
cstr_split_with_seperator(const char *sourceString, const char *seperator)
{
  std::stringstream ss(sourceString);
  std::string token;
  std::vector<std::string> tokens;
  while (std::getline(ss, token, *seperator))
    {
      tokens.push_back(token);
    }
  return tokens;
}

int
cstr_is_numeric(const char *s)
{
  char *ptr;
  if (s == nullptr || *s == '\0' || isspace(*s)) return 0;

  strtod(s, &ptr);
  return *ptr == '\0';
}

/* To replace a single char with another single char in a given string */

void
cstr_replace_char(char *str_in, char orig_char, char rep_char)
{

  char *ref = nullptr;

  if (DBG) ref = str_in;

  if (strchr(str_in, orig_char) == nullptr) return;

  if (DBG) fprintf(stderr, "Before %s\n", ref);

  while (*str_in != '\0')
    {
      if (*str_in == orig_char) *str_in = rep_char;
      str_in++;
    }
  if (DBG) fprintf(stderr, "After %s\n", ref);

  return;
}
