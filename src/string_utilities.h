/*
  This file is part of CDO. CDO is a collection of Operators to manipulate and analyse Climate model Data.

  Author: Uwe Schulzweida
          Oliver Heidmann

*/
#ifndef STR_UTILITIES_H
#define STR_UTILITIES_H

#include <vector>
#include <string>

std::vector<std::string> cstr_split_with_seperator(const char *sourceString, const char *seperator);

int cstr_is_numeric(const char *s);

void cstr_replace_char(char *str_in, char orig_char, char rep_char);

#endif
