#ifndef TEMPLATE_PARSER_HH
#define TEMPLATE_PARSER_HH

int init_XML_template_parser(char *Filename);
int updatemagics_and_results_nodes(void);
int quit_XML_template_parser(void);

#endif
