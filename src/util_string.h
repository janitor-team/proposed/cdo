#ifndef UTIL_STRING_H
#define UTIL_STRING_H

#include <string>
#include <vector>

#define ADD_PLURAL(n) ((n) != 1 ? "s" : "")

std::vector<std::string> split_string(const std::string &str, const std::string &regex_str);

std::string string_to_upper(std::string p_str);
std::string string_to_lower(std::string p_str);

void cstr_to_lower_case(char *str);
void cstr_to_upper_case(char *str);
char *double_to_att_str(int digits, char *str, size_t len, double value);

const char *tunit_to_cstr(int tunits);
const char *calendar_to_cstr(int calendar);

std::string get_scientific(double p_float_string);

#endif
